import path from 'path';
import { createServer } from 'http';
import express from 'express';
import request from 'request';
import cheerio from 'cheerio';

const app = express();

if (!process.env.PORT) throw new Error('Environment variable PORT not defined');
const PORT = process.env.PORT;

app.use('/', (req, res) => {
  console.log('url:', req.url);

  if (req.url === '/') {
    request('https://www.exponea.com', (error, response, body) => {
      const $ = cheerio.load(body);
      const scriptNode = '<script type="text/javascript" src="/__webpack/bundle.js"></script>';
      const rootNode = '<div id="wysiwyg-editor-root"></div>';
      $('head').prepend(scriptNode);
      $('body').append(rootNode);
      res.send($.html());
    });
  } else {
    request(`https://www.exponea.com${req.url}`).pipe(res);
  }
});

const server = createServer();

server.on('request', app);

server.listen(PORT, (error) => {
  if (error) {
    console.error(error);
    return;
  }

  console.log(`Listening at http://*:${PORT}`);
});

const signalHandler = () => {
  console.log('Stopping server');
  server.close(() => {
    process.exit(0);
  });
};

process.on('SIGTERM', signalHandler);
process.on('SIGINT', signalHandler);
