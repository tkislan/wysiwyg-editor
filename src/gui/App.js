import React from 'react';

import Paper from 'material-ui/Paper';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

import { rootStyle } from './App.scss';

function getLocator(el) {
  if (!el) return [];
  return [...getLocator(el.parentElement), el.localName];
}

function insertIntoArray(arr, value) {
  return arr.reduce((result, element, index, array) => {
    if (index < array.length - 1) {
      return [...result, element, value];
    } else {
      return [...result, element];
    }
  }, []);
};

export default class App extends React.PureComponent {
  state = {
    contextMenuOpen: false,
    contextMenuAnchorEl: null,
    locator: [],
    dialogOpen: false,
    newOuterHTML: '',
  };

  componentDidMount() {
    window.addEventListener('contextmenu', this.handleRightClick, false);
  }

  handleRightClick = (e) => {
    e.preventDefault();
    // alert('success!');
    console.log(e.target);
    console.log(e);

    console.log(getLocator(e.target));

    this.setState({ 
      contextMenuOpen: true,
      contextMenuAnchorEl: e.target,
      locator: getLocator(e.target),
    });

    return false;
  };

  handleCloseContextMenu = () => {
    this.setState({ contextMenuOpen: false, contextMenuAnchorEl: null, locator: [] });
  };

  handleOpenOuterHTMLDialog = () => {
    this.setState({ contextMenuOpen: false, dialogOpen: true, newOuterHTML: this.state.contextMenuAnchorEl.outerHTML });
  };

  handleCloseDialog = () => {
    this.setState({ dialogOpen: false });
  };

  handleSaveHtml = () => {
    this.state.contextMenuAnchorEl.outerHTML = this.state.newOuterHTML;
    this.handleCloseDialog();
  };

  handleSetNewOuterHTML = (e) => {
    this.setState({ newOuterHTML: e.target.value });
  };

  handleRemoveElement = () => {
    const el = this.state.contextMenuAnchorEl;
    this.handleCloseContextMenu();
    el.remove();
  };

  render() {
    const { contextMenuOpen, contextMenuAnchorEl, locator, dialogOpen, newOuterHTML } = this.state;

    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleCloseDialog}
      />,
      <FlatButton
        label="Save"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleSaveHtml}
      />,
    ];

    return (
      <Paper className={rootStyle}>
        <Popover
          open={contextMenuOpen}
          anchorEl={contextMenuAnchorEl}
          onRequestClose={this.handleCloseContextMenu}
        >
          <Menu>
            <MenuItem primaryText="Edit text" />
            <MenuItem primaryText="Edit HTML" onTouchTap={this.handleOpenOuterHTMLDialog} />
            <MenuItem primaryText="Edit CSS" />
            <MenuItem primaryText="Remove" onTouchTap={this.handleRemoveElement} />
          </Menu>
        </Popover>
        <Dialog
          title="Dialog With Actions"
          actions={actions}
          modal={false}
          open={dialogOpen}
          onRequestClose={this.handleCloseDialog}
        >
          <TextField
            hintText="outerHTML"
            multiLine={true}
            fullWidth
            rows={10}
            rowsMax={20}
            value={newOuterHTML}
            onChange={this.handleSetNewOuterHTML}
          />
        </Dialog>
        <div className="toolbar">
          Wysiwyg editor
        </div>
        <div className="locator">
          {
            insertIntoArray(locator, '>').map((l, i) => (
              <span key={i}>{l}</span>
            ))
          }
        </div>
      </Paper>
    );
  }
}
