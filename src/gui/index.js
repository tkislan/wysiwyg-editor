import React from 'react';
import { render } from 'react-dom';

import injectTapEventPlugin from 'react-tap-event-plugin';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { addLocaleData } from 'react-intl';
import enLocaleData from 'react-intl/locale-data/en';

import App from './App';

import './index.scss';

if (!window.tapEventInjected) {
  injectTapEventPlugin();
  window.tapEventInjected = true;
}

if (!window.localeDataAdded) {
  addLocaleData(enLocaleData);
  window.localeDataAdded = true;
}

console.log('Wysiwyg editor loaded');

window.addEventListener('load', () => {
  document.body.style.marginTop = '100px';

  const rootEl = document.getElementById('wysiwyg-editor-root');


  render(<MuiThemeProvider><App /></MuiThemeProvider>, rootEl);
}, false);

if (module.hot) {
  module.hot.accept((error) => {
    if (error) {
      console.error('Cannot apply hot update:', error);
    }
  });
}
