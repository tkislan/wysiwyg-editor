import path from 'path';
import webpack from 'webpack';
import baseConfig from './webpack.gui.config.base';

const config = {
  ...baseConfig,

  devtool: 'source-map',

  entry: [
    './src/gui/index.js',
  ],

  output: {
    ...baseConfig.output,
    path: '/__webpack',
    publicPath: '/__webpack',
  },

  module: {
    ...baseConfig.module,
    rules: [
      ...baseConfig.module.rules,
      {
        test: /\.jsx?$/,
        use: ['react-hot-loader', 'babel-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              localIdentName: '[path][name]__[local]--[hash:base64:5]',
            },
          },
          'sass-loader',
        ],
      },
    ],
  },

  plugins: [
    ...baseConfig.plugins,
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      __DEV__: true,
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
      },
    }),
  ],

  devServer: {
    // allowedHosts: ['*'],
    host: '0.0.0.0',
    compress: true,
    port: 3004,
    historyApiFallback: true,
    hot: true,
    // watchOptions: { poll: true },
    proxy: {
      '/': 'http://localhost:3000',
    },
  },
};

export default config;
