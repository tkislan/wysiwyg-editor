export default {
  module: {
    rules: [],
    // noParse: /node_modules\/json-schema\/lib\/validate\.js/,
    noParse: /validate\.js/, //TODO: rewrite more specifically  (due to inner validate.js dep https://github.com/kriszyp/json-schema/issues/59)
  },
  output: {
    filename: 'bundle.js',
  },
  resolve: {
    extensions: ['.js'],
  },
  plugins: [],
  externals: [
    // put your node 3rd party libraries which can't be built with webpack here
    // (mysql, mongodb, and so on..)
  ],
};
