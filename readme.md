# How to run

`npm install`

`npm run start-dev`

With custom hostname:<br/>
`HOSTNAME="https://www.kistler.com" npm run start-dev`

By default, `https://www.exponea.com` is used